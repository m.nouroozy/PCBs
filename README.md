# Chordata PCBs
_The Chordata PCBs and components libraries_

This hardware pieces make up the physical part of the [Chordata open source motion capture system](http://chordata.cc)


![all kceptor revisions](KCeptor_history.jpg)